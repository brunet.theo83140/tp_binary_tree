#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
défine the TreeNode class for the binary_tree
"""

class TreeNode:
    """
    Binary node object
    parameters : Data : value of the node
    """



    def __init__(self, data):
        self.data = data
        self.right_child = None
        self.left_child = None
        self.parent = None

    def is_leaf(self):
        """
        confirm if the node is a leaf
        """
        return self.right_child is None and self.left_child is None

    def __str__(self):
        if self.is_leaf():
            return str(self.data)
        return "["+ str(self.left_child) +";"+ str(self.right_child) +"]:"+ str(self.data)

    def set_child(self, child_node):
        """
        add a child to the node (if there is a empty link)
        """
        child_node.parent = self
        if self.right_child is None:
            self.right_child = child_node
        elif self.left_child is None:
            self.left_child = child_node

    def find_structure(self):
        """
        return the tree below this node in a list
        """
        if self.is_leaf():
            return [self.data, [self.right_child, self.left_child]]
        if self.right_child is None:
            return [self.data, [self.right_child, self.left_child.find_structure()]]
        if self.left_child is None:
            return [self.data, [self.right_child.find_structure(), self.left_child]]
        return [self.data, \
        [self.right_child.find_structure(), self.left_child.find_structure()]]

    def is_not_full(self):
        """
        return if the node has an empty link
        """
        return self.right_child is None or self.left_child is None




    def find_the_path_to_first_none(self):
        """
        return the shortest path to a empty link (a str with 0 for left and 1 for right)
        """
        if not self.is_not_full():
            if self.right_child.is_not_full():
                return str('1')
            if self.left_child.is_not_full():
                return str('0')
            r_path = '1' + self.right_child.find_the_path_to_first_none()
            l_path = '0' + self.left_child.find_the_path_to_first_none()
            if len(l_path) < len(r_path):
                return l_path
            return r_path
        return ''

    def delete(self):
        """
        remove the parameter of the node
        """
        self.data = None
        self.left_child = None
        self.right_child = None
        self.parent = None
