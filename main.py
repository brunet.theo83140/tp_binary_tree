#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This scipt is a test for tree_node and binary_tree
"""

from tree_node import TreeNode
from binary_tree import Tree

if __name__ == "__main__":
    n0 = TreeNode('noeud 1')
    n1 = TreeNode('noeud 2')
    n2 = TreeNode('noeud 3')
    n3 = TreeNode('noeud 4')
    n4 = TreeNode('noeud 5')
    n5 = TreeNode('noeud 6')
    n6 = TreeNode('noeud 7')
    n7 = TreeNode("noeud 8")
    n8 = TreeNode("noeud 9")
    n9 = TreeNode("noeud 10")
    n10 = TreeNode("noeud 11")
    arbre = Tree(n0)
    arbre.add_node(n1, n0)
    arbre.add_node(n2, n0)
    arbre.add_node(n3, n0)
    arbre.add_node(n4, n0)
    arbre.add_node(n5, n0)
    arbre.add_node(n6, n0)
    arbre.add_node(n7, n0)
    arbre.add_node(n8, n0)

    print('[Before delete] :\t\t' + str(arbre.root_node) + '\n')

    arbre.delete_node(n5)

    print('[After delete of node 6] :\t' + str(arbre.root_node) + '\n')

    arbre.delete_node(n0)

    print('[delete of node 1 (root)] :\t' + str(arbre.root_node) + '\n')

    arbre.add_node(n9, n6)

    print('[add node 10 after node 7] :\t' + str(arbre.root_node) + '\n')

    arbre.add_node(n10, n4)

    print('[add node 11 after node 5] :\t' + str(arbre.root_node) + '\n')

    print('[transversal deep] :\t\t' + str(arbre.traversal_deep()) + '\n')

    test = Tree.create_tree_with_list([1, 2, 3, 5, 6, 8, 9, 7, 4, 5, 2, 1])

    print('[tree create with a list] :\t' + str(test.root_node) + '\n')

    print('[is_leaf on node 11] :\t\t' + str(n10.is_leaf()) +  '\n')

    print('[is_leaf on node 1] :\t\t' + str(n0.is_leaf()) + '\n')

    n12 = TreeNode('noeud 1')
    n13 = TreeNode('noeud 2')
    n14 = TreeNode('noeud 3')
    n15 = TreeNode('noeud 4')

    arbre_2 = Tree(n12)
    arbre_2.add_node(n13, n12)
    arbre_2.add_node(n14, n13)
    arbre_2.add_node(n15, n13)

    print('[tree with one branch] : \t' + str(arbre_2.root_node) + '\n')
    arbre_2.delete_node(n12)
    print('[delete of the root] : \t\t' + str(arbre_2.root_node) + '\n')

    n16 = TreeNode('noeud 1')
    arbre_3 = Tree(n16)

    print('[tree with one node] : \t\t' + str(arbre_3.root_node) + '\n')
    arbre_3.delete_node(n16)
    print('[delete a tree with one node] : +' + str(arbre_3.root_node) + '\n')
