#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Class for the binary tree (unsorted)
"""
from tree_node import TreeNode

class Tree:
    """
    Binary Tree object
    parameter : root_node = first node of the Tree
    """
    def __init__(self: object, root_node: object):
        self.root_node = root_node

    @staticmethod
    def create_tree_with_list(list_value: list):
        """
        function to create a Tree object en fill it with a list
        """
        for i in list_value:
            node = TreeNode(i)
            if 'arbre' not in locals():
                arbre = Tree(node)
            else:
                arbre.add_node(node, arbre.root_node)
        return arbre


    def traversal_deep(self):
        """
        return the topology of the tree in a list
        """
        return str(self.root_node)

    def add_node(self, added_node, target_node, path=None):
        """
        insert a node in the first avaible place behind the tardet node
        follow the path return by find_the_path_to_first_none to use set_child
        """
        if path is None:
            path = target_node.find_the_path_to_first_none()
            self.add_node(added_node, target_node, path)
        else:
            if path != '':
                if path[0] == '1':
                    self.add_node(added_node, target_node.right_child, path[1:])
                elif path[0] == '0':
                    self.add_node(added_node, target_node.left_child, path[1:])
            else:
                target_node.set_child(added_node)

    def delete_node(self, target_node):
        """
        delete the target node
        """
        if target_node.parent is not None:
            parent = target_node.parent
            if parent.right_child == target_node:
                if target_node.right_child is not None:
                    parent.right_child = target_node.right_child
            else:
                if target_node.left_child is not None:
                    parent.left_child = target_node.right_child
        if target_node.right_child is not None and target_node.left_child is not None:
            self.add_node(target_node.left_child, target_node.right_child)
            if target_node == self.root_node:
                self.root_node = target_node.right_child
        if target_node == self.root_node:
            if target_node.right_child is None:
                self.root_node = target_node.left_child
            elif target_node.left_child is None:
                self.root_node = target_node.right_child
            else:
                self.root_node = None
        target_node.delete()
